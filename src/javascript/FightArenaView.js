import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { createImage } from './fighterView';
//import versus from '../../resources/img/versus.jpg';

export function createArena(choosedfighters) {
  const root = document.getElementById('root');
  const arena = createElement({ tagName: 'div', className: 'arena-root' });
  const healthIndicators = createHealthBlock(...choosedfighters);
  const fighters = createFighters(...choosedfighters);
  
  arena.append(healthIndicators, fighters);
  root.innerHTML = '';
  root.append(arena);
  const [ leftFighter, rightFighter ] = choosedfighters;
  showWinnerModal(fight(leftFighter, rightFighter));
}

function createHealthBlock(leftFighter, rightFighter) {

  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left-indicator');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right-indicator');

  const healthIndicators = createElement({ tagName: 'div', className: 'arena-gamestatus' });
  healthIndicators.append(leftFighterIndicator,  rightFighterIndicator);

  return healthIndicators;
}

function createHealthIndicator(fighter, id) {
  const { name } = fighter;
  const indicatorcontainer = createElement({ tagName: 'div', className: 'arena-fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena-fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena-health-indicator' });
  const bar = createElement({ tagName: 'div', className: 'arena-health-bar',
   attributes: {id:`${id}`}});
  fighterName.innerText = name;
  indicator.append(bar);
  indicatorcontainer.append(fighterName, indicator);

  return indicatorcontainer;
}

function createFighters(firstFighter, secondFighter) {
  const gamefield = createElement({ tagName: 'div', className: `arena-gamefield` });
  const leftFighterElement = createFighter(firstFighter, 'left');
  const rightFighterElement = createFighter(secondFighter, 'right');
 // const versus = createVersusBlock()

  gamefield.append(leftFighterElement, rightFighterElement);

  return gamefield;
}

function createFighter(fighter, position) {
  const {source} = fighter;
  const imgElement = createImage(source,'arena-fighter-img');
  const positionClassName = position === 'left'?'displayfighters-fighter':
  'displayfighters-fighter-right'
  const fighterElement = createElement({
    tagName: 'div',
    className: `${positionClassName}`,
  });
  fighterElement.append(imgElement);

  return fighterElement;
}
/*function createVersusBlock()
{
  return createImage(
    versus,
    'arena-versus-img'
  );
}*/
