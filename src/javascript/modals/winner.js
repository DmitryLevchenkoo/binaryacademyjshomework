import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';
import  {startApp}  from '../app';

export function showWinnerModal(fighter) {
  const hintElement = createElement({ tagName: 'span', className: 'modal__hint' });
  hintElement.innerText = `Fighter ${fighter.name} WIN`;
  const bodyElement = createElement({ tagName: 'div', className: 'modal__body' });
  bodyElement.append(hintElement);
  return showModal({ title: 'Game Over', onClose: onModalClose, bodyElement });
}

function onModalClose(){
  const arenaElement = document.querySelector('.arena-root');
  arenaElement.remove();
  const start = startApp();
  return start;
}