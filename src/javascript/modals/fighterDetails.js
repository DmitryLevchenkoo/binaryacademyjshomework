import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  const details = Object.keys(fighter).map(characteristic => {
    const detailElement = createElement({ tagName: "div", className: "fighter-detail" });
    detailElement.innerText = characteristic + ": " + fighter[characteristic];
    return detailElement;
  });
  const detailsList = createElement({ tagName: "div", className: "fighter-details" });
  detailsList.append(...details);
  nameElement.innerText = name;
  fighterDetails.append(nameElement,detailsList);

  return fighterDetails;
}
