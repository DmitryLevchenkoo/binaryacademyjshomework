
export function fight(firstFighter, secondFighter) {
  const firstHealthIndicator = document.getElementById('left-indicator');
  const secondHealthIndicator = document.getElementById('right-indicator');

  const fighterOneStarterHealth = firstFighter.health;
  const fighterTwoStarterHealth = secondFighter.health;

  let isfirst = false;

  //let timer = Date.now();
  firstFighter = { ...firstFighter };
  secondFighter = { ...secondFighter };

  while (true) {
    if (firstFighter.health <= 0) {
      return secondFighter;
    } else if (secondFighter.health <= 0) {
      return firstFighter;
    }
    if (isfirst) {
      secondFighter.health -= getDamage(firstFighter, secondFighter);
      isfirst = false;
    }
    else {
      isfirst = true;
      firstFighter.health -= getDamage(secondFighter, firstFighter);
    }
    updateHealthIndicators();
  }

  function updateHealthIndicators() {
    firstHealthIndicator.style.width = calculateIndicatorHealthWidth(fighterOneStarterHealth, firstFighter.health) + "%";
    secondHealthIndicator.style.width = calculateIndicatorHealthWidth(fighterTwoStarterHealth, secondFighter.health) + "%";
  }
  
}
export function getDamage(attacker, enemy) {

  const blockPower = getBlockPower(enemy);
  const hitPower = getHitPower(attacker);

  const damage = hitPower - blockPower;

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {

  const { attack } = fighter;
  return attack * (1 + Math.random());

}

export function getBlockPower(fighter) {

  const { defense } = fighter;
  return defense * (1 + Math.random());

}

function calculateIndicatorHealthWidth(starterHealth, currentHealth) {

  const width = 100 * currentHealth / starterHealth;
  return width < 0 ? 0 : width;

}
